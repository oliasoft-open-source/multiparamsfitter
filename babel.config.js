module.exports = (api) => {
  api.cache(true);

  const presets = [
    [
      '@babel/preset-env',
      {
        targets: {
          browsers: ['defaults'],
        },
        useBuiltIns: 'usage',
        corejs: 3,
      },
    ],
    '@babel/preset-react',
  ];

  const plugins = [
    '@babel/plugin-proposal-class-properties',
    'babel-plugin-transform-md-import-to-string',
    [
      'babel-plugin-webpack-aliases',
      { config: './webpack/webpack.config.js' },
    ],
    'add-module-exports',
  ];

  return {
    presets,
    plugins,
  };
};
