const path = require('path');

module.exports = function getRules(env) {
  return [
    {
      test: /\.jsx$|\.es6$|\.js$/,
      exclude: /(node_modules|oliasoftDefaultLibraries)/,
      use: {
        loader: 'babel-loader',
        options: {
          cacheDirectory: true,
          presets: ['@babel/env'],
        },
      },
    },
  ];
};
