const getRules = require('./webpack.common.rules.js');

module.exports = {
  mode: 'production',
  devtool: 'source-map',
  entry: {
    dist: './src/mpfit.js'
  },
  resolve: {
    extensions: ['.js'],
  },
  module: {
    rules: getRules('development')
  },
  output: {
    library: '@oliasoft-open-source/multiparamsfitter',
    libraryTarget: 'umd',
    globalObject: "typeof self !== 'undefined' ? self : this"
  }
};
