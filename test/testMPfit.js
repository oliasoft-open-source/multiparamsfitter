// Javascript
// @ts-check
// Translated and updated, by permission from C. Markwardt, by Truls M. Larsen 2020 (C).
// Program code provided as-is with no warranty. TML
//
// Some original commets from C library
//
// MINPACK-1 Least Squares Fitting Library
//
// Test routines
//
// These test routines provide examples for users to familiarize
// themselves with the mpfit library.  They also provide a baseline
// test data set for users to be sure that the library is functioning
// properly on their platform.
//
// Original C code Copyright (C) 2003,2006,2009,2010, Craig Markwardt
//
// Tranlation (C) 2020 Truls M. Larsen
// Exp functon added by Truls M. Larsen 2020
//

import mpFitSimple, { mpFit, MPParameter, MPConfig, MPResult } from '../src/mpfit';

/* This is the private data structure which contains the data points
   and their uncertainties */

class DataPoints {
  /**
   * @param {Number[]} xPoints
   * @param {Number[]} yPoints
   * @param {Number[]} uncY
   */
  constructor(xPoints = null, yPoints = null, uncY = null) {
    this.x = xPoints;
    this.y = yPoints;
    this.ey = uncY;
    if (this.ey === null)
      this.ey = this.y.map(y => y * 0);;
  }
}

/* Simple routine to print the fit results */
/**
 * @param {String} name
 * @param {Number} code
 * @param {String} info
 * @param {Number[]} x
 * @param {Number[]} xact
 * @param {MPResult} result
 */
function printresult(name, code, info, x, xact, result) {
  if (!x || !result) return;
  console.log('*** %s status = %d\n*** %s', name, code, info);

  console.log('  CHI-SQUARE = %f    (%d DOF)\n',
    result.chi2, result.nResiduals - result.nFreePars);
  console.log('        NPAR = %d\n', result.nParams);
  console.log('       NFREE = %d\n', result.nFreePars);
  console.log('     NPEGGED = %d\n', result.nPeggedPars);
  console.log('       NITER = %d\n', result.niter);
  console.log('        NFEV = %d (+ %d)\n', result.nFuncEvals, result.nFuncMemo);
  console.log('\n');

  if (xact)
    for (let i = 0; i < result.nParams; i++)
      console.log('  P[%d] = %f +/- %f     (ACTUAL %f)\n',
        i, x[i], result.params[i].uncert, xact[i]);
  else
    for (let i = 0; i < result.nParams; i++)
      console.log('  P[%d] = %f +/- %f\n',
        i, x[i], result.params[i].uncert);

  console.log(result);
}

/**
 * linear fit function
 * @returns{Number} - error code (0 = success)
 *
 * @param {Number[]} p - array of fit parameters
 * @param {Number[]} dy - array of residuals to be returned
 * @param {Number[][]} dvec
 * @param {Object} vars - private data (struct vars_struct *)
 */
function linfunc(p, dy, dvec, vars) {
  for (let i = 0; i < vars.y.length; i++) {
    dy[i] = (vars.y[i] - (p[0] + p[1] * vars.x[i])) / (vars.ey[i] != 0 ? vars.ey[i] : 1);
  }
  // console.log(dy);
  // console.log("\n %f - %f x\n", p[0], p[1]);
  return 0;
}

// Test harness routine, which contains test data, invokes mpfit()
function testlinfit() {
  const x = [
    -1.7237128E+00, 1.8712276E+00, -9.6608055E-01, -2.8394297E-01, 1.3416969E+00,
    1.3757038E+00, -1.3703436E+00, 4.2581975E-02, -1.4970151E-01, 8.2065094E-01];

  const y = [
    1.9000429E-01, 6.5807428E+00, 1.4582725E+00, 2.7270851E+00, 5.5969253E+00,
    5.6249280E+00, 0.787615, 3.2599759E+00, 2.9771762E+00, 4.5936475E+00];

  let v = new DataPoints(x, y);
  for (let i = 0; i < 10; i++)
    v.ey[i] = 0.07;   /* Data errors */

  let pars = [1.0, 1.0];          /* Parameter initial conditions */
  const pactual = [3.20, 1.78];   /* Actual values used to make data */

  let parConstraints = [];
  for (let i = 0; i < pars.length; i++) {
    parConstraints.push(new MPParameter());
    parConstraints[i].side = 2;
    parConstraints[i].lowLimited = false;
    parConstraints[i].hiLimited = false;
    parConstraints[i].lowLimit = 0;
    parConstraints[i].hiLimit = 0;
    parConstraints[i].value = pars[i];
  }

  let conf = null; // new MPConfig();
  // conf.chi2Tolerance = -1;

  //parConstraints = null;
  // Call fitting function for 10 data points and 2 parameters
  let status = mpFit(linfunc, x.length, parConstraints, conf, v);
  printresult('testlinfit', status.code, status.info, status.params, pactual, status.result);
}

/**
 * quadratic fit function
 *
 * RETURNS: error code (0 = success)
 *
 * @param {Number[]} p - array of fit parameters
 * @param {Number[]} dy - array of residuals to be returned
 * @param {Number[][]} dvec - optional analysical derivative matrix
 * @param {object} vars - private data
 */
function quadfunc(p, dy, dvec, vars) {
  // console.log ('quadfunc %f %f %f\n', p[0], p[1], p[2]);
  for (let i = 0; i < vars.y.length; i++)
    dy[i] = (vars.y[i] - p[0] - p[1] * vars.x[i] - p[2] * vars.x[i] * vars.x[i])
      / (vars.ey[i] > 0 ? vars.ey[i] : 1);

  return 0;
}

// Test harness routine, which contains test quadratic data, invokes mpfit()
function testquadfit() {
  const x = [
    -1.7237128E+00, 1.8712276E+00, -9.6608055E-01, -2.8394297E-01, 1.3416969E+00,
    1.3757038E+00, -1.3703436E+00, 4.2581975E-02, -1.4970151E-01, 8.2065094E-01];

  const y = [
    2.3095947E+01, 2.6449392E+01, 1.0204468E+01, 5.40507, 1.5787588E+01,
    1.6520903E+01, 1.5971818E+01, 4.7668524E+00, 4.9337711E+00, 8.7348375E+00];

  let v = new DataPoints(x, y);
  for (let i = 0; i < x.length; i++)
    v.ey[i] = 0.2;       /* Data errors */

  let pars = [1.0, 1.0, 1.0];       /* Initial conditions */
  const pactual = [4.7, 0.0, 6.2];  /* Actual values used to make data */

  let parConstraints = [];
  for (let i = 0; i < pars.length; i++) {
    parConstraints.push(new MPParameter());
    parConstraints[i].value = pars[i];
  }

  // Call fitting function for 10 data points and 3 parameters
  let status = mpFit(quadfunc, x.length, parConstraints, null, v);
  printresult('testquadfit', status.code, status.info, status.params, pactual, status.result);
}

// Test harness routine, which contains test quadratic data;
// Example of how to fix a parameter
function testquadfix() {
  const x = [
    -1.7237128E+00, 1.8712276E+00, -9.6608055E-01, -2.8394297E-01, 1.3416969E+00,
    1.3757038E+00, -1.3703436E+00, 4.2581975E-02, -1.4970151E-01, 8.2065094E-01];

  const y = [
    2.3095947E+01, 2.6449392E+01, 1.0204468E+01, 5.40507, 1.5787588E+01,
    1.6520903E+01, 1.5971818E+01, 4.7668524E+00, 4.9337711E+00, 8.7348375E+00];

  let v = new DataPoints(x, y);
  for (let i = 0; i < x.length; i++)
    v.ey[i] = 0.2;

  let pars = [1.0, 0.0, 1.0];         /* Initial conditions */
  const pactual = [4.7, 0.0, 6.2]; /* Actual values used to make data */

  let parConstraints = [];
  for (let i = 0; i < pars.length; i++) {
    parConstraints.push(new MPParameter());
    parConstraints[i].value = pars[i];
  }
  // Fixed parameter 1
  parConstraints[1].fixed = true;

  // Call fitting function for 10 data points and 3 parameters (1 parameter fixed)
  let status = mpFit(quadfunc, x.length, parConstraints, null, v);
  printresult('testquadfix', status.code, status.info, status.params, pactual, status.result);
}

/**
 * gaussian fit function
 *
 * @returns{Number} - error code (0 = success)
 *
 * @param {Number[]} p - array of fit parameters
 *                       p[0] = constant offset
 *                       p[1] = peak y value
 *                       p[2] = x centroid position
 *                       p[3] = gaussian sigma width
 * @param {Number[]} dy - array of residuals to be returned
 * @param {Number[][]} dvec
 * @param {any} vars - private data
 */
function gaussfunc(p, dy, dvec, vars) {
  const sig2 = p[3] * p[3];

  for (let i = 0; i < vars.y.length; i++) {
    const xc = vars.x[i] - p[2];
    dy[i] = (vars.y[i] - p[1] * Math.exp(-0.5 * xc * xc / sig2) - p[0])
      / (vars.ey[i] > 0 ? vars.ey[i] : 1);
  }

  return 0;
}

// Test harness routine, which contains test gaussian-peak data
function testgaussfit() {
  const x = [
    -1.7237128E+00, 1.8712276E+00, -9.6608055E-01, -2.8394297E-01, 1.3416969E+00,
    1.3757038E+00, -1.3703436E+00, 4.2581975E-02, -1.4970151E-01, 8.2065094E-01];

  let y = [
    -4.4494256E-02, 8.7324673E-01, 7.4443483E-01, 4.7631559E+00, 1.7187297E-01,
    1.1639182E-01, 1.5646480E+00, 5.2322268E+00, 4.2543168E+00, 6.2792623E-01];

  let pars = [0.0, 1.0, 1.0, 1.0];           /* Initial conditions */
  const pactual = [0.0, 4.70, 0.0, 0.5]; /* Actual values used to make data*/

  let v = new DataPoints(x, y);
  for (let i = 0; i < x.length; i++)
    v.ey[i] = 0; // 0.5;

  let parConstraints = [];
  for (let i = 0; i < pars.length; i++) {
    parConstraints.push(new MPParameter());
    // parConstraints[i].side = 2;
    // parConstraints[i].relDeltaStep /= 10;
    parConstraints[i].value = pars[i];
  }

  /* Call fitting function for 10 data points and 4 parameters (no parameters fixed) */
  let status = mpFit(gaussfunc, x.length, parConstraints, null, v);
  printresult('testgaussfit', status.code, status.info, status.params, pactual, status.result);
}


// Test harness routine, which contains test gaussian-peak data
// Example of fixing two parameter
function testgaussfix() {
  const x = [
    -1.7237128E+00, 1.8712276E+00, -9.6608055E-01, -2.8394297E-01, 1.3416969E+00,
    1.3757038E+00, -1.3703436E+00, 4.2581975E-02, -1.4970151E-01, 8.2065094E-01];

  const y = [
    -4.4494256E-02, 8.7324673E-01, 7.4443483E-01, 4.7631559E+00, 1.7187297E-01,
    1.1639182E-01, 1.5646480E+00, 5.2322268E+00, 4.2543168E+00, 6.2792623E-01];

  let v = new DataPoints(x, y);
  for (let i = 0; i < x.length; i++)
    v.ey[i] = 0.5;

  let pars = [0.0, 1.0, 0.0, 0.1];       /* Initial conditions */
  const pactual = [0.0, 4.70, 0.0, 0.5]; /* Actual values used to make data*/

  let parConstraints = [];
  for (let i = 0; i < pars.length; i++) {
    parConstraints.push(new MPParameter());
    parConstraints[i].value = pars[i];
  }

  // Fix parameters 0 and 2
  parConstraints[0].fixed = true;
  parConstraints[2].fixed = true;
  // parConstraints[3].limited[0] = 0;
  // parConstraints[3].limited[1] = 1;
  // parConstraints[3].limits[0] = -0.3;
  // parConstraints[3].limits[1] = +0.2;

  // Call fitting function for 10 data points and 4 parameters (2 parameters fixed)
  let status = mpFit(gaussfunc, x.length, parConstraints, null, v);
  printresult('testgaussfix', status.code, status.info, status.params, pactual, status.result);
}

/**
 * Exponential fit function
 *
 * @returns{Number} - error code (0 = success)
 *
 * @param {Number[]} p - array of fit parameters *
 * @param {Number[]} dy - array of residuals to be returned
 * @param {Number[][]} dvec
 * @param {any} vars - private data
 */
function expFunc(p, dy, dvec, vars) {
  // [0]+[1]*x^[2]
  for (let i = 0; i < vars.y.length; i++)
    dy[i] = (vars.y[i] - (p[0] + p[1] * Math.pow(vars.x[i], p[2])))
      / (vars.ey[i] > 0 ? vars.ey[i] : 1);
  return 0;
}

//_____________________________________________________________________________
function testExp() {
  const x = [5.112, 10.224, 51.12, 102.24, 170.4, 340.8, 511.2, 1022.4];
  const y = [5.8765, 6.3875, 9.2491, 11.9574, 15.0234, 21.5131, 27.083, 41.391];
  let ey = y.map(y => y * 0.000);

  let v = new DataPoints(x, y, ey);

  let pars = [1, 0.5, 0.5];       /* Initial conditions */
  const pactual = [5.08404, 0.242764, 0.722623]; /* Actual values */
  const pactualUnc = [0.00693486, 0.000762881, 0.000445006];
  // with uncertainties: 0.00693486, 0.000762881, 0.000445006

  let parConstraints = [];
  for (let i = 0; i < pars.length; i++) {
    parConstraints.push(new MPParameter());
    parConstraints[i].value = pars[i];
    parConstraints[i].lowLimit = 0;
    parConstraints[i].lowLimited = true;
    // parConstraints[i].deltaStep = 1e-10;
    // parConstraints[i].relDeltaStep = 1e-10;
  }

  parConstraints[2].hiLimited = true;
  parConstraints[2].hiLimit = 1;

  let conf = new MPConfig();
  // conf.chi2Tolerance = 1e-12;
  // conf.paramTolerance = 1e-12
  // conf.orthoTolerance = 1e-12;
  // conf.covTolerance = 1e-15;
  conf.hasResidUncert = false;

  let status = mpFit(expFunc, x.length, parConstraints, conf, v);
  // let status = mpFitSimple(expFunc, x.length, pars, v);

  printresult('testExp', status.code, status.info, status.params, pactual, status.result);
  console.log(" *** Root uncertainties:\n dP[0]: %f\n dP[1]: %f\n dP[2]: %f\n",
    pactualUnc[0], pactualUnc[1], pactualUnc[2]);
}

// Run all tests
export default function runTest() {
  testlinfit();
  testquadfit();
  testquadfix();
  testgaussfit();
  testgaussfix();
  testExp();
}

//_____________________________________________________________________________
//
// EOF
//
