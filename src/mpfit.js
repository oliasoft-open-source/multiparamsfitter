// Javascript
// @ts-check
// Translated again to Javascript by Truls M, Larsen
// Created: 2018-11-10
// Creator: Truls M. Larsen truls@oliasoft.com / trulstrengerikkealias@gmail.com
//
// Orignal comments and previous translations is below.
// Text that is not related to javascript may have been deleted for clarity.
// Text related to the javascript is added where relevant.

/*
 * MINPACK-1 Least Squares Fitting Library
 *
 * Original public domain version by B. Garbow, K. Hillstrom, J. More'
 *   (Argonne National Laboratory, MINPACK project, March 1980)
 * See the file DISCLAIMER for copyright information.
 *
 * Tranlation to C Language by S. Moshier (moshier.net)
 *
 * Enhancements and packaging by C. Markwardt
 *   (comparable to IDL fitting routine MPFIT
 *    see http://cow.physics.wisc.edu/~craigm/idl/idl.html)
 */
/* Main mpfit library routines (double precision)
   $Id: mpfit.c,v 1.24 2013/04/23 18:37:38 craigm Exp $
 *
 *     The purpose of mpfit is to minimize the sum of the squares of
 *     m nonlinear functions in n variables by a modification of
 *     the levenberg-marquardt algorithm. the user must provide a
 *     subroutine which calculates the functions. the jacobian is
 *     then calculated by a finite-difference approximation.
 *
 *     mp_funct funct - function to be minimized
 *     double *xall   - array of n initial parameter values
 *                      upon return, contains adjusted parameter values
 *     mp_par *pars   - array of npar structures specifying constraints;
 *                      or 0 (null pointer) for unconstrained fitting
 *                      [ see README and mpfit.h for definition & use of mp_par]
 *     mp_config *config - pointer to structure which specifies the
 *                      configuration of mpfit(); or 0 (null pointer)
 *                      if the default configuration is to be used.
 *                      See README and mpfit.h for definition and use
 *                      of config.
 *     void *private  - any private user data which is to be passed directly
 *                      to funct without modification by mpfit().
 *     mp_result *result - pointer to structure, returned, contains
 *                      the results of the fit.  The user should zero this
 *                      structure.  If any of the array values are to be
 *                      returned, the user should allocate storage for them
 *                      and assign the corresponding pointer in *result.
 *                      Upon return, *result will be updated, and
 *                      any of the non-null arrays will be filled.
 *
 * FORTRAN DOCUMENTATION BELOW
 *
 *     the subroutine statement is
 *
 *     subroutine lmdif(fcn,m,n,x,fvec,ftol,xtol,gtol,maxfev,epsfcn,
 *                diag,mode,factor,nprint,info,nfev,fjac,
 *                ldfjac,ipvt,qtf,wa1,wa2,wa3,wa4)
 *
 *     where
 *
 *     fcn is the name of the user-supplied subroutine which
 *       calculates the functions. fcn must be declared
 *       in an external statement in the user calling
 *       program, and should be written as follows.
 *
 *       subroutine fcn(m,n,x,fvec,iflag)
 *       integer m,n,iflag
 *       double precision x(n),fvec(m)
 *       ----------
 *       calculate the functions at x and
 *       return this vector in fvec.
 *       ----------
 *       return
 *       end
 *
 *       the value of iflag should not be changed by fcn unless
 *       the user wants to terminate execution of lmdif.
 *       in this case set iflag to a negative integer.
 *
 *     m is a positive integer input variable set to the number
 *       of functions/data points.
 *
 *     x is an array of length n. on input x must contain
 *       an initial estimate of the solution vector. on output x
 *       contains the final estimate of the solution vector.
 *
 *     fvec is an output array of length m which contains
 *       the functions evaluated at the output x.
 *
 *     ftol is a nonnegative input variable. termination
 *       occurs when both the actual and predicted relative
 *       reductions in the sum of squares are at most ftol.
 *       therefore, ftol measures the relative error desired
 *       in the sum of squares.
 *
 *     xtol is a nonnegative input variable. termination
 *       occurs when the relative error between two consecutive
 *       iterates is at most xtol. therefore, xtol measures the
 *       relative error desired in the approximate solution.
 *
 *     gtol is a nonnegative input variable. termination
 *       occurs when the cosine of the angle between fvec and
 *       any column of the jacobian is at most gtol in absolute
 *       value. therefore, gtol measures the orthogonality
 *       desired between the function vector and the columns
 *       of the jacobian.
 *
 *     maxfev is a positive integer input variable. termination
 *       occurs when the number of calls to fcn is at least
 *       maxfev by the end of an iteration.
 *
 *     epsfcn is an input variable used in determining a suitable
 *       step length for the forward-difference approximation. this
 *       approximation assumes that the relative errors in the
 *       functions are of the order of epsfcn. if epsfcn is less
 *       than the machine precision, it is assumed that the relative
 *       errors in the functions are of the order of the machine
 *       precision.
 *
 *     diag is an array of length n. if mode = 1 (see
 *       below), diag is internally set. if mode = 2, diag
 *       must contain positive entries that serve as
 *       multiplicative scale factors for the variables.
 *
 *     mode is an integer input variable. if mode = 1, the
 *       variables will be scaled internally. if mode = 2,
 *       the scaling is specified by the input diag. other
 *       values of mode are equivalent to mode = 1.
 *
 *     factor is a positive input variable used in determining the
 *       initial step bound. this bound is set to the product of
 *       factor and the euclidean norm of diag*x if nonzero, or else
 *       to factor itself. in most cases factor should lie in the
 *       interval (.1,100.). 100. is a generally recommended value.
 *
 *     nprint is an integer input variable that enables controlled
 *       printing of iterates if it is positive. in this case,
 *       fcn is called with iflag = 0 at the beginning of the first
 *       iteration and every nprint iterations thereafter and
 *       immediately prior to return, with x and fvec available
 *       for printing. if nprint is not positive, no special calls
 *       of fcn with iflag = 0 are made.
 *
 *     info is an integer output variable. if the user has
 *       terminated execution, info is set to the (negative)
 *       value of iflag. see description of fcn. otherwise,
 *       info is set as follows.
 *
 *       info = 0  improper input parameters.
 *
 *       info = 1  both actual and predicted relative reductions
 *              in the sum of squares are at most ftol.
 *
 *       info = 2  relative error between two consecutive iterates
 *              is at most xtol.
 *
 *       info = 3  conditions for info = 1 and info = 2 both hold.
 *
 *       info = 4  the cosine of the angle between fvec and any
 *              column of the jacobian is at most gtol in
 *              absolute value.
 *
 *       info = 5  number of calls to fcn has reached or
 *              exceeded maxfev.
 *
 *       info = 6  ftol is too small. no further reduction in
 *              the sum of squares is possible.
 *
 *       info = 7  xtol is too small. no further improvement in
 *              the approximate solution x is possible.
 *
 *       info = 8  gtol is too small. fvec is orthogonal to the
 *              columns of the jacobian to machine precision.
 *
 *     nfev is an integer output variable set to the number of
 *       calls to fcn.
 *
 *     fjac is an output m by n array. the upper n by n submatrix
 *       of fjac contains an upper triangular matrix r with
 *       diagonal elements of nonincreasing magnitude such that
 *
 *          p * (jac^2) * p = r^2
 *
 *       where p is a permutation matrix and jac is the final
 *       calculated jacobian. column j of p is column ipvt(j)
 *       (see below) of the identity matrix. the lower trapezoidal
 *       part of fjac contains information generated during
 *       the computation of r.
 *
 *     ldfjac is a positive integer input variable not less than m
 *       which specifies the leading dimension of the array fjac.
 *
 *     ipvt is an integer output array of length n. ipvt
 *       defines a permutation matrix p such that jac*p = q*r,
 *       where jac is the final calculated jacobian, q is
 *       orthogonal (not stored), and r is upper triangular
 *       with diagonal elements of nonincreasing magnitude.
 *       column j of p is column ipvt(j) of the identity matrix.
 *
 *     qtf is an output array of length n which contains
 *       the first n elements of the vector (q transpose)*fvec.
 *
 *     wa1, wa2, and wa3 are work arrays of length n.
 *
 *     wa4 is a work array of length m.
 *
 *     subprograms called
 *
 *     user-supplied ...... fcn
 *
 *     minpack-supplied ... dpmpar,enorm,fdjac2,lmpar,qrfac
 *
 *     fortran-supplied ... dabs,dmax1,dmin1,dsqrt,mod
 *
 *     argonne national laboratory. minpack project. march 1980.
 *     burton s. garbow, kenneth e. hillstrom, jorge j. more
 *
 */

//=============================================================================
/* Error codes */
export const kMPFitStatus = Object.freeze({
  MPFIT_VERSION: '0.0.1',
  MP_GOOD: 0,              /* Calculation is a good state */
  MP_ERR_INPUT: -1,        /* General input parameter error */
  MP_ERR_NAN: -16,         /* User function produced non-finite values */
  MP_ERR_FUNC: -17,        /* No user function was supplied */
  MP_ERR_NPOINTS: -18,     /* No user data points were supplied */
  MP_ERR_NFREE: -19,       /* No free parameters */
  MP_ERR_INITBOUNDS: -21,  /* Initial values inconsistent w constraints*/
  MP_ERR_BOUNDS: -22,      /* Initial constraints inconsistent */
  MP_ERR_PARAM: -23,       /* General input parameter error */
  MP_ERR_DOF: -24,         /* Not enough degrees of freedom */
  MP_ERR_CONF: -25,        /* Error in the configuration object */

  /* Potential success status codes */
  MP_OK_CHI: 1,            /* Convergence in chi-square value */
  MP_OK_PAR: 2,            /* Convergence in parameter value */
  MP_OK_BOTH: 3,           /* Both MP_OK_PAR and MP_OK_CHI hold */
  MP_OK_DIR: 4,            /* Convergence in orthogonality */
  MP_MAXITER: 5,           /* Maximum number of iterations reached */
  MP_CHI2TOL: 6,           /* ftol is too small, no further improvement possible */
  MP_PARAMTOL: 7,          /* xtol is too small, no further improvement possible */
  MP_ORTHOTOL: 8          /* gtol is too small; no further improvement possible */
});

/* Double precision numeric constants */
const MP_MACHEP0 = 2.2204460e-16;
const MP_DWARF = Number.MIN_VALUE; // 2.2250739e-308;
const MP_GIANT = Number.MAX_VALUE; // 1.7976931e+308;

const MP_RDWARF = Math.sqrt(MP_DWARF * 1.5) * 10;
const MP_RGIANT = Math.sqrt(MP_GIANT) * 0.1;

const kInfoText = new Array(9);
kInfoText[kMPFitStatus.MP_ERR_INPUT] = 'Improper input parameters: No data and no parameters!';
kInfoText[kMPFitStatus.MP_OK_CHI] = 'Both actual and predicted relative reductions in the sum of squares are at most chi^2 tolerance.';
kInfoText[kMPFitStatus.MP_OK_PAR] = 'Relative error between two consecutive iterates is at most parameter tolerance.';
kInfoText[kMPFitStatus.MP_OK_BOTH] = 'Conditions for chi^2 tolerance and parameter tolerance both hold.';
kInfoText[kMPFitStatus.MP_OK_DIR] = 'The cosine of the angle between fvec and any column of the jacobian is at most gtol in absolute value.';
kInfoText[kMPFitStatus.MP_MAXITER] = 'Warning: Number of calls to user function has reached or exceeded maximum.';
kInfoText[kMPFitStatus.MP_CHI2TOL] = '(chi^2 is smaller than tolerance) No further reduction in the chi-square is possible.';
kInfoText[kMPFitStatus.MP_PARAMTOL] = '(Parameter changes are smaller than tolerance) No further improvement in the parameters are possible.';
kInfoText[kMPFitStatus.MP_ORTHOTOL] = '(Orthogonality within tolerance) Residuals vector is orthogonal to the columns of the jacobian matrix.';

kInfoText[kMPFitStatus.MP_ERR_NAN] = 'User function produced non-finite values.';
kInfoText[kMPFitStatus.MP_ERR_FUNC] = 'No propper user function was supplied.';
kInfoText[kMPFitStatus.MP_ERR_NPOINTS] = 'No user data points were supplied.';
kInfoText[kMPFitStatus.MP_ERR_NFREE] = 'There are no free parameters.';
kInfoText[kMPFitStatus.MP_ERR_INITBOUNDS] = 'Initial values inconsistent w constraints.';
kInfoText[kMPFitStatus.MP_ERR_BOUNDS] = 'Initial constraints are inconsistent with initial values.';
kInfoText[kMPFitStatus.MP_ERR_PARAM] = 'General input parameter error.';
kInfoText[kMPFitStatus.MP_ERR_DOF] = 'Not enough degrees of freedom.';
kInfoText[kMPFitStatus.MP_ERR_CONF] = 'Error in the configuration object.';


/** @param {Number} code */
export function getMPError(code) {
  return (kInfoText[code] ? kInfoText[code] : 'No info found for error code: ' + code);
}

/**
 * @param {String} txt
 * @param {Array} args
 */
function printConsoleError(txt, ...args) {
  console.log('%c *** ' + txt, 'color: #AA0000; font-weight: bold;', ...args);
}
/**
 * @param {String} txt
 * @param {Array} args
 */
function printConsoleMsg(txt, ...args) {
  console.log('%c *** ' + txt, 'color: #009900; ; font-style: italic;', ...args);
}

//=============================================================================
export class MPParameter {
  // Object holdeing a fit parameter, including constraints etc
  constructor() {
    this.value = undefined;  /** Set to guestimate. Contains fitted value after fit */
    this.uncert = undefined; /** The uncertainty of this parameter after fit */
    this.fixed = false;      /** true: fixed; false: free */
    this.lowLimited = false; /** true: has lower limit; false: no limit */
    this.hiLimited = false;  /** true: has upper limit; false: no limit */
    this.lowLimit = Number.MAX_VALUE; /** Parameter lower limit */
    this.hiLimit = Number.MAX_VALUE;  /** Parameter upper limit */
    this.pegged = false; /** on return set to true if value is equal to low or hi limit */

    this.parname = '';  /** Name of parameter, or 0 for none */
    this.deltaStep = 0; /** Step size for finite difference specific for this parameter
                         * the forward-difference approximation. This
                         * approximation assumes that the relative errors in the
                         * functions are of the order of epsfcn. If epsfcn is less
                         * than the machine precision, it is assumed that the relative
                         * errors in the functions are of the order of the machine precision.
                         */
    this.relDeltaStep = 0; /** Relative step size for finite difference for this parameter */

    this.side = -1;     /** Sidedness of finite difference derivative
                         * 0 - one-sided derivative computed automatically
                         * 1 - one-sided derivative:  (f(x+h) - f(x))/h
                         * -1 - one-sided derivative: (f(x)  - f(x-h))/h
                         * 2 - two-sided derivative:  (f(x+h) - f(x-h))/(2*h)
                         * 3 - user-computed analytical derivatives
                         */
    this.deriv_debug = false;  /** Derivative debug mode: 1 = Yes; 0 = No;
                                *
                                * If yes, compute both analytical and numerical
                                * derivatives and print them to the console for
                                * comparison.
                                *
                                * NOTE: when debugging, do *not* set side = 3,
                                * but rather to the kind of numerical derivative
                                * you want to compare the user-analytical one to
                                * (0, 1, -1, or 2).
                                */
    // this.deriv_reltol = 1e-10; /* Relative tolerance for derivative debug printout */
    // this.deriv_abstol = 1e-10; /* Absolute tolerance for derivative debug printout */
  }

  //_____________________________________________________________________________
  withinBounds() {
    if (this.fixed) return true;

    if (this.lowLimited && this.hiLimited && this.lowLimit > this.hiLimit)
      return false;

    if ((this.lowLimited && this.value < this.lowLimit)
      || (this.hiLimited && this.value > this.hiLimit))
      return false;

    return true;
  }
}

//=============================================================================
// Fitting configuration parameters
export class MPConfig {
  constructor() {
    this.memoize = true;         /** true: memoize user func result for each call with a set of parameters */
    this.hasResidUncert = true;  /** false: parameter uncertainties will be scaled with chi^2/nDoF */
    this.chi2Tolerance = 1e-10;  /** Relative chi-square convergence criterium. DEFAULT: 1e-10
                                  * Is a nonnegative input variable. Termination
                                  * occurs when both the actual and predicted relative
                                  * reductions in the sum of squares are at most this value.
                                  * Therefore, this value measures the relative error
                                  * desired in the sum of squares.
                                  */
    this.paramTolerance = 1e-10; /** Relative parameter convergence criterium. DEFAULT: 1e-10
                                  * Is a nonnegative input variable. Termination occurs when
                                  * the relative error between two consecutive iterates is
                                  * at most this value. Therefore, this value measures the
                                  * relative error desired in the approximate solution.
                                  */
    this.orthoTolerance = 1e-10; /** Orthogonality convergence criterium. DEFAULT: 1e-10
                                  * Is a nonnegative input variable. Termination occurs when
                                  * the cosine of the angle between residuals vector  and any
                                  * column of the jacobian is at most this value in absolute
                                  * terms. Therefore, this value measures the orthogonality
                                  * desired between the function vector and the columns
                                  * of the jacobian.
                                  */
    this.covTolerance = 1e-14; /** Range tolerance for covariance calculation Default: 1e-14 */

    this.epsfcn = MP_MACHEP0; /** Finite derivative step size for all params that don't have one. DEFAULT: MP_MACHEP0
                               * Variable used in determining a suitable step length for the
                               * forward-difference approximation. This approximation assumes
                               * that the relative errors in the functions are of the order of
                               * epsfcn. If epsfcn is less than the machine precision, it is
                               * assumed that the relative errors in the functions are of the
                               * order of the machine precision.
                               */
    this.stepfactor = 100.0;   /** Initial step bound recommmended range [0.1, 100] - Default: 100 */

    this.maxiter = 200; /** Maximum number of iterations.  If maxiter == MP_NO_ITER,
                         * then basic error checking is done, and parameter
                         * errors/covariances are estimated based on input
                         * parameter values, but no fitting iterations are done.
                         * Default: 200 */
    // this.MP_NO_ITER = -1; /** No iterations, just checking */
    this.maxFuncEval = 0; /** Maximum number of function evaluations, or 0 for no limit Default: 0 (no limit) */

    this.nprint = 0;      /* Default: 1 */
    this.doUserScale = false; /** Scale variables by user values?
                               * true = yes, user scale values in diag;
                               * false = no, variables scaled internally (Default)
                               */
    this.finiteCheck = false; /** Disable check for infinite quantities from user?
                               * false = do not perform check (Default)
                               * true = perform check
                               */
  }

  //_____________________________________________________________________________
  checkCongfig() {
    if (this.chi2Tolerance <= 0) {
      printConsoleError('Chi^2 tolerance cannot be 0 or negative: %f', this.chi2Tolerance);
      return false;
    }
    if (this.paramTolerance <= 0) {
      printConsoleError('Parameter tolerance cannot be 0 or negative: %f', this.paramTolerance);
      return false;
    }
    if (this.orthoTolerance <= 0) {
      printConsoleError('Orthogonal tolerance cannot be 0 or negative: %f', this.orthoTolerance);
      return false;
    }
    if (this.covTolerance <= 0) {
      printConsoleError('Covariance tolerance cannot be 0 or negative: %f', this.covTolerance);
      return false;
    }

    return true;
  }
}

//=============================================================================
export class MPResult {
  /** @param{Number} nParameters */
  constructor(nParameters) {
    this.chi2 = Number.MAX_VALUE;         /** Final chi^2 */
    this.chi2Start = Number.MAX_VALUE;    /** Starting value of chi^2 */
    this.niter = 0;                       /** Number of iterations */
    this.nFuncEvals = 0;                  /** Number of function evaluations */
    this.nFuncMemo = 0;                   /** Number of function call from memoization */
    this.status = kMPFitStatus.MP_OK_CHI; /** Fitting status code */

    this.nParams = nParameters; /** Total number of parameters */
    this.nFreePars = 0;         /** Number of free parameters */
    this.nPeggedPars = 0;       /** Number of pegged parameters */
    this.nResiduals = 0;        /** Number of residuals (= num. of data points) */
    this.nDof = 0;
    /** @type{Number[]} */
    this.residuals = new Array(this.nParams);  /** Final residuals nfunc-vector, or 0 if not desired */
    /** @type{Number[]} */
    this.covar = new Array(this.nParams ** 2); /** Final parameter covariance matrix npar x npar array, or 0 if not desired */

    this.residuals.fill(NaN);
    this.covar.fill(NaN);

    /** @type{MPParameter[]} */
    this.params = null; /** array of the resulting parameters */

    this.version = '';  /** MPFIT version string */
  }

  //_____________________________________________________________________________
  getResultText() {
    return kInfoText[this.status] ? kInfoText[this.status] : 'No info found for error code: ' + this.status;
  }

  //_____________________________________________________________________________
  /** @param {Boolean} longFormat */
  print(longFormat = false) {
    printConsoleMsg('CHI-SQUARE = %.2f (nDOF = %d, chi2/nDOF = %.2f)',
      this.chi2, this.nResiduals - this.nFreePars,
      this.chi2 / (this.nResiduals - this.nFreePars));
    if (longFormat) {
      printConsoleMsg('      Nparams = %d', this.nParams);
      printConsoleMsg('        Nfree = %d', this.nFreePars);
      printConsoleMsg('      Npegget = %d', this.nPeggedPars);
      printConsoleMsg('        Niter = %d', this.niter);
      printConsoleMsg('   NfuncEvals = %d (+ %d memoized)', this.nFuncEvals, this.nFuncMemo);
    }

    for (let i = 0; i < this.nParams; i++)
      printConsoleMsg('  %s = %f %s',
        (this.params[i].parname.length > 0 ? this.params[i].parname : 'P[' + i + ']'),
        this.params[i].value,
        (this.params[i].fixed ? 'fixed' : (this.params[i].pegged ? 'pegged' : '+/- ' + this.params[i].uncert)));
  }
}

//=============================================================================
export class MPFit {
  /**
   * @param {Function} userFunction - pointer to the user supplied function that computes
   *                                  residuals and optionally analysic derivatives
   * @param {MPParameter[]} pars - array of function parameters
   * @param {Boolean} [useMemoization]
   */
  constructor(userFunction, pars, useMemoization = false) {
    /** @type{function} */
    this._userFuncPointer = userFunction;
    this._nFuncEvals = 0;

    /** @type{MPParameter[]} */
    this.pars = pars;

    /** @type{MPParameter[]} */
    this.freePars = null;

    /** @type{Number[]} */
    this.idxFree = null;

    /** @type{MPParameter[]} */
    this.fixedPars = null;

    /** @type{Number[]} */
    this.idxFixed = null;

    /** @type{MPConfig} */
    this.conf = null;

    /** @type{Array<{pars: Number[], residuals: Number[], derivVec: Number[][], retVal: Number}>} */
    this._userFuncStored = []; // for memoization
    this._nStoreRetrievals = 0;
    this._useMemo = useMemoization;
    this._userFuncErrorCode = 0;

    /** @type{MPResult} */
    this.result = null;


    this.init();
  }

  //_____________________________________________________________________________
  init() {
    if (!this.pars) return;

    // initalize all internal arrays
    this.freePars = [];
    this.fixedPars = [];
    this.idxFree = [];
    this.idxFixed = [];
    this._userFuncStored = [];
    this._nStoreRetrievals = 0;

    this.pars.forEach((par, idx) => {
      if (par.fixed) {
        this.fixedPars.push(par);
        this.idxFixed.push(idx);
      } else {
        this.freePars.push(par);
        this.idxFree.push(idx);
      }
    });
  }

  /** Add the result from calling the user function, for memoiztion purpose
   * @param {Number} retVal
   * @param {Number[]} pars
   * @param {Number[]} residuals
   * @param {Number[][]} derivVec
   */
  addToStore(retVal, pars, residuals, derivVec) {
    const parCopy = pars.map(x => x);
    const residCopy = residuals.map(x => x);

    const dvCopy = (!derivVec ? null : derivVec.map(x => (x.map(y => y))));
    this._userFuncStored.push({ pars: parCopy, residuals: residCopy, derivVec: dvCopy, retVal: retVal });
  }

  /** Wrapped user function call, to enable memoization
   * @param {Number[]} pars - parameters
   * @param {Number[]} residuals - this array will be filled with user calculated residuals
   * @param {Number[][]} derivVec - this array will be filled with user calculated derivatives
   * @param {Object} privData - pass on private data to the user function
   */
  userFunc(pars, residuals, derivVec, privData) {
    if (this._userFuncPointer === undefined)
      return kMPFitStatus.MP_ERR_FUNC;

    // check if we have a stored result
    if (this._useMemo) {
      const result = this._userFuncStored.find(stored => {
        for (let i = 0; i < pars.length; i++)
          if (pars[i] != stored.pars[i])
            return false;

        return true;
      });
      if (result) {
        residuals = result.residuals.map(x => x);
        derivVec = (result.derivVec ? result.derivVec.map(x => (x.map(y => y))) : null);
        this._nStoreRetrievals++;
        console.log(' === Fetched a stored result! (Store size %d)', this._userFuncStored.length);
        this._userFuncErrorCode = result.retVal
        return (result.retVal === kMPFitStatus.MP_GOOD);
      }
    }
    // not found so calculate it
    this._nFuncEvals++;
    this._userFuncErrorCode = this._userFuncPointer(pars, residuals, derivVec, privData);

    // check and stpore result
    if (this.conf.finiteCheck && residuals.find(val => !isFinite(val)) !== undefined) {
      this._userFuncErrorCode = kMPFitStatus.MP_ERR_NAN;
      return false;
    }

    if (this._useMemo)
      this.addToStore(this._userFuncErrorCode, pars, residuals, derivVec);
    return (this._userFuncErrorCode === kMPFitStatus.MP_GOOD);
  }

  /************************lmmisc.c*************************/
  /** Given an m by n matrix a, an n by n diagonal matrix d,
   * and an m-vector b, the problem is to determine an x which
   * solves the system
   *
   *     a * x = b   d * x = 0
   *
   * in the least squares sense.
   *
   * this subroutine completes the solution of the problem
   * if it is provided with the necessary information from the
   * qr factorization, with column pivoting, of a. that is, if
   * a*p = q*r, where p is a permutation matrix, q has orthogonal
   * columns, and r is an upper triangular matrix with diagonal
   * elements of nonincreasing magnitude, then qrsolv expects
   * the full upper triangle of r, the permutation matrix p,
   * and the first n components of (q transpose)*b. the system
   * a*x = b, d*x = 0, is then equivalent to
   *
   *     r * z = q * b ,  p * d * p * z = 0 ,
   *
   * where x = p*z. if this system does not have full rank,
   * then a least squares solution is obtained. on output qrsolv
   * also provides an upper triangular matrix s such that
   *
   *     p * (a^2 + d^2) * p = s^2
   *
   * s is computed within qrsolv and may be of separate interest.
   *
   * argonne national laboratory. minpack project. march 1980.
   * burton s. garbow, kenneth e. hillstrom, jorge j. more
   *
   * @param {Number} n - is a positive integer input variable set to the order of r.
   * @param {Number[]} r - is an n by n array. on input the full upper triangle
   *                  must contain the full upper triangle of the matrix r.
   *                  ON OUTPUT the full upper triangle is unaltered, and the
   *                  strict lower triangle contains the strict upper triangle
   *                  (transposed) of the upper triangular matrix s.
   * @param {Number} ldr - is a positive integer input variable not less than n
   *                    which specifies the leading dimension of the array r.
   * @param {Number[]} ipvt - is an integer input array of length n which defines the
   *                     permutation matrix p such that a*p = q*r. column j of p
   *                     is column ipvt(j) of the identity matrix.
   * @param {Number[]} diag - is an input array of length n which must contain the
   *                     diagonal elements of the matrix d.
   * @param {Number[]} qtb - is an input array of length n which must contain the first
   *                    n elements of the vector (q transpose)*b.
   * @param {Number[]} x - is an OUTPUT ARRAY of length n which contains the least
   *                  squares solution of the system a*x = b, d*x = 0.
   * @param {Number[]} sdiag - is an OUTPUT ARRAY of length n which contains the
   *                      diagonal elements of the upper triangular matrix s.
   */
  mpQRSolve(n, r, ldr, ipvt, diag, qtb, x, sdiag) {
    // copy r and (q transpose)*b to preserve input and initialize s.
    // in particular, save the diagonal elements of r in x.
    let wa = qtb.map(val => val);
    for (let j = 0, kk = 0; j < n; j++, kk += ldr + 1) {
      for (let i = j, ij = kk, ik = kk; i < n; i++, ij++, ik += ldr)
        r[ij] = r[ik];

      x[j] = r[kk];
    }

    // eliminate the diagonal matrix d using a givens rotation.
    for (let j = 0; j < n; j++) {
      // prepare the row of d to be eliminated, locating the
      // diagonal element using p from the qr factorization.
      if (diag[ipvt[j]] != 0) {
        sdiag.fill(0, j, j + n);

        sdiag[j] = diag[ipvt[j]];

        // the transformations to eliminate the row of d
        // modify only a single element of (q transpose)*b
        // beyond the first n, which is initially zero.
        let qtbpj = 0;
        for (let k = j; k < n; k++) {
          // determine a givens rotation which eliminates the
          // appropriate element in the current row of d.
          if (sdiag[k] == 0)
            continue; // eslint-disable-line no-continue

          const kk = k + ldr * k;
          let sinx, cosx;
          if (Math.abs(r[kk]) < Math.abs(sdiag[k])) {
            let cotan = r[kk] / sdiag[k];
            sinx = 0.5 / Math.sqrt(0.25 + 0.25 * cotan ** 2);
            cosx = sinx * cotan;
          } else {
            let tanx = sdiag[k] / r[kk];
            cosx = 0.5 / Math.sqrt(0.25 + 0.25 * tanx ** 2);
            sinx = cosx * tanx;
          }

          // compute the modified diagonal element of r and
          // the modified element of ((q transpose)*b,0).
          r[kk] = cosx * r[kk] + sinx * sdiag[k];
          let temp = cosx * wa[k] + sinx * qtbpj;
          qtbpj = -sinx * wa[k] + cosx * qtbpj;
          wa[k] = temp;
          // accumulate the tranformation in the row of s.
          if (n > k + 1)
            for (let i = k + 1, ik = kk + 1; i < n; i++, ik++) {
              temp = cosx * r[ik] + sinx * sdiag[i];
              sdiag[i] = -sinx * r[ik] + cosx * sdiag[i];
              r[ik] = temp;
            }
        }
      }

      // store the diagonal element of s and restore
      // the corresponding diagonal element of r.
      const kk = j + ldr * j;
      sdiag[j] = r[kk];
      r[kk] = x[j];
    }

    // solve the triangular system for z. if the system is
    // singular, then obtain a least squares solution. */
    let nsing = n;
    for (let j = 0; j < n; j++) {
      if (sdiag[j] == 0 && nsing == n)
        nsing = j;
      if (nsing < n)
        wa[j] = 0;
    }

    if (nsing >= 1) {
      for (let k = 0; k < nsing; k++) {
        const j = nsing - k - 1;
        let sum = 0;

        if (nsing > j + 1)
          for (let i = j + 1, ij = j + 1 + ldr * j; i < nsing; i++, ij++)
            sum += r[ij] * wa[i];

        wa[j] = (wa[j] - sum) / sdiag[j];
      }
    }

    // permute the components of z back to components of x.
    wa.forEach((val, idx) => { x[ipvt[idx]] = val; });

    // last card of subroutine qrsolv - TML comment: Once translated from "punch-cards"
    // return the modified input parameters
    return { r, x, sdiag };
  }

  /************************covar.c************************
   *
   * Given an m by n matrix a, the problem is to determine
   * the covariance matrix corresponding to a, defined as
   *
   *       inverse(a * a)
   *
   * this subroutine completes the solution of the problem
   * if it is provided with the necessary information from the
   * qr factorization, with column pivoting, of a. that is, if
   * a*p = q*r, where p is a permutation matrix, q has orthogonal
   * columns, and r is an upper triangular matrix with diagonal
   * elements of nonincreasing magnitude, then covar expects
   * the full upper triangle of r and the permutation matrix p.
   * the covariance matrix is then computed as
   *
   *       p*inverse(r * r) * p
   *
   * if a is nearly rank deficient, it may be desirable to compute
   * the covariance matrix corresponding to the linearly independent
   * columns of a. to define the numerical rank of a, covar uses
   * the tolerance tol. if l is the largest integer such that
   *
   *       abs(r(l, l)) > tol * abs(r(1, 1))
   *
   * then covar computes the covariance matrix corresponding to
   * the first l columns of r. for k greater than l, column
   * and row ipvt(k) of the covariance matrix are set to zero.
   *
   * argonne national laboratory. minpack project. august 1980.
   * burton s. garbow, kenneth e. hillstrom, jorge j. more
   *
   * @param {Number} n - is a positive integer input variable set to the order of r.
   * @param {Number[]} r  - is an n by n array. on input the full upper triangle must
   *                          contain the full upper triangle of the matrix r. ON OUTPUT
   *                          r contains the square symmetric covariance matrix.
   * @param {Number} ldr - is a positive integer input variable not less than n
   *                       which specifies the leading dimension of the array r.
   * @param {Number[]} ipvt - is an integer input array of length n which defines the
   *                          permutation matrix p such that a*p = q*r. column j of p
   *                          is column ipvt(j) of the identity matrix.
   * @param {Number} tol - is a nonnegative input variable used to define the
   *                       numerical rank of a in the manner described above.
   */
  mpCovariance(n, r, ldr, ipvt, tol) {
    // debugging
    const DEBUG_MP_COVAR = false;
    if (DEBUG_MP_COVAR) {
      console.log('Prior to start of mp_covar:');
      for (let j = 0; j < n; j++)
        for (let i = 0; i < n; i++)
          console.log(r[j * ldr + i]);
    }

    // form the inverse of r in the full upper triangle of r.
    const tolr = tol * Math.abs(r[0]);
    let l = -1;
    for (let k = 0; k < n; k++) {
      const kk = k * ldr + k;
      if (Math.abs(r[kk]) <= tolr) break;

      r[kk] = 1 / r[kk];
      for (let j = 0; j < k; j++) {
        const kj = k * ldr + j;
        const temp = r[kk] * r[kj];
        r[kj] = 0;

        const k0 = k * ldr;
        const j0 = j * ldr;
        for (let i = 0; i <= j; i++)
          r[k0 + i] += -temp * r[j0 + i];
      }
      l = k;
    }

    // Form the full upper triangle of the inverse of (r transpose)*r
    // in the full upper triangle of r
    if (l >= 0) {
      for (let k = 0; k <= l; k++) {
        const k0 = k * ldr;

        for (let j = 0; j < k; j++) {
          const temp = r[k * ldr + j];
          for (let i = 0; i <= j; i++)
            r[j * ldr + i] += temp * r[k0 + i];
        }

        const temp = r[k0 + k];
        for (let i = 0; i <= k; i++)
          r[k0 + i] *= temp;
      }
    }

    // For the full lower triangle of the covariance matrix
    // in the strict lower triangle or and in wa
    let wa = new Array(n);
    for (let j = 0; j < n; j++) {
      const jj = ipvt[j];
      const j0 = j * ldr;
      const jj0 = jj * ldr;
      for (let i = 0; i <= j; i++) {
        const ji = j0 + i;

        if (j > l) r[ji] = 0;
        const ii = ipvt[i];
        if (ii > jj) r[jj0 + ii] = r[ji];
        if (ii < jj) r[ii * ldr + jj] = r[ji];
      }
      wa[jj] = r[j0 + j];
    }

    // Symmetrize the covariance matrix in r
    for (let j = 0; j < n; j++) {
      for (let i = 0; i < j; i++)
        r[j * ldr + i] = r[i * ldr + j];

      r[j * ldr + j] = wa[j];
    }

    // debugging
    if (DEBUG_MP_COVAR) {
      console.log('Afer cmpletion of mp_covar:');
      for (let j = 0; j < n; j++)
        for (let i = 0; i < n; i++)
          console.log(r[j * ldr + i]);
    }

    return r;
  }

  /************************enorm.c*************************/
  /** Given an n-vector x, this function calculates the
   * euclidean norm of x.
   *
   * the euclidean norm is computed by accumulating the sum of
   * squares in three different sums. the sums of squares for the
   * small and large components are scaled so that no overflows
   * occur. non-destructive underflows are permitted. underflows
   * and overflows do not occur in the computation of the unscaled
   * sum of squares for the intermediate components.
   * the definitions of small, intermediate and large components
   * depend on two constants, rdwarf and rgiant. the main
   * restrictions on these constants are that rdwarf**2 not
   * underflow and rgiant**2 not overflow. the constants
   * given here are suitable for every known computer.
   *
   * argonne national laboratory. minpack project. march 1980.
   * burton s. garbow, kenneth e. hillstrom, jorge j. more
   *
   * @param {Number} n - is a positive integer input variable.
   * @param {Number[]} x - is an input array of length n.
   */
  mpEuclidNorm(n, x) {
    let s1 = 0;
    let s2 = 0;
    let s3 = 0;
    let x1max = 0;
    let x3max = 0;

    for (let i = 0; i < n; i++) {
      const xabs = Math.abs(x[i]);
      if (xabs > MP_RDWARF && xabs < MP_RGIANT / n) { // sum for intermediate components.
        s2 += xabs * xabs;
        continue; // eslint-disable-line no-continue
      } else if (xabs > MP_RDWARF) { // sum for large components.
        if (xabs > x1max) {
          s1 = 1.0 + s1 * (x1max / xabs) ** 2;
          x1max = xabs;
        } else
          s1 += (xabs / x1max) ** 2;

        continue; // eslint-disable-line no-continue
      } else if (xabs > x3max) { // sum for small components.
        s3 = 1.0 + s3 * (x3max / xabs) ** 2;
        x3max = xabs;
      } else if (xabs != 0)
        s3 += (xabs / x3max) ** 2;
    }

    // calculation of norm.
    if (s1 != 0)
      return x1max * Math.sqrt(s1 + (s2 / x1max) / x1max);

    if (s2 != 0)
      return Math.sqrt(s2 >= x3max
        ? s2 * (1.0 + (x3max / s2) * (x3max * s3))
        : x3max * ((s2 / x3max) + (x3max * s3)));

    return x3max * Math.sqrt(s3);
    // last card of function enorm. TML comment: Once upon a time, translated from "punch cards"!
  }

  /************************mp_lmpar.c*************************/
  /** Given an m by n matrix a, an n by n nonsingular diagonal
   * matrix d, an m-vector b, and a positive number delta,
   * the problem is to determine a value for the parameter
   * par such that if x solves the system
   *
   *    a * x = b   sqrt(par) * d * x = 0
   *
   * in the least squares sense, and dxnorm is the euclidean
   * norm of d*x, then either par is zero and
   *
   *    (dxnorm - delta) <= 0.1 * delta
   *
   * or par is positive and
   *
   *    abs(dxnorm - delta) <= 0.1 * delta
   *
   * this subroutine completes the solution of the problem
   * if it is provided with the necessary information from the
   * qr factorization, with column pivoting, of a. that is, if
   * a*p = q*r, where p is a permutation matrix, q has orthogonal
   * columns, and r is an upper triangular matrix with diagonal
   * elements of nonincreasing magnitude, then lmpar expects
   * the full upper triangle of r, the permutation matrix p,
   * and the first n components of (q transpose)*b. on output
   * lmpar also provides an upper triangular matrix s such that
   *
   *    p * (a^2 + par * d^2) * p = s^2
   *
   * s is employed within lmpar and may be of separate interest.
   *
   * only a few iterations are generally needed for convergence
   * of the algorithm. if, however, the limit of 10 iterations
   * is reached, then the output par will contain the best
   * value obtained so far.
   *
   *     argonne national laboratory. minpack project. march 1980.
   *     burton s. garbow, kenneth e. hillstrom, jorge j. more
   *
   * @param{Number} n - is a positive integer input variable set to the order of r.
   * @param{Number[]} r - is an n by n array. on input the full upper triangle
   *                        must contain the full upper triangle of the matrix r.
   *                        ON OUTPUT the full upper triangle is unaltered, and the
   *                        strict lower triangle contains the strict upper triangle
   *                        (transposed) of the upper triangular matrix s.
   * @param{Number} ldr - is a positive integer input variable not less than n
   *                      which specifies the leading dimension of the array r.
   * @param{Number[]} ipvt - is an integer input array of length n which defines the
   *                         permutation matrix p such that a*p = q*r. column j of p
   *                         is column ipvt(j) of the identity matrix.
   * @param{Number[]} diag - is an input array of length n which must contain the
   *                        diagonal elements of the matrix d.
   * @param{Number[]} qtb - is an input array of length n which must contain the first
   *                       n elements of the vector (q transpose)*b.
   * @param{Number} delta - is a positive input variable which specifies an upper
   *                       bound on the euclidean norm of d*x.
   * @param{Number} par - is a nonnegative variable. on input par contains an
   *                     initial estimate of the levenberg-marquardt parameter.
   *                     ON OUTPUT par contains the final estimate.
   * @param{Number[]} x - is an OUTPUT ARRAY of length n which contains the least
   *                     squares solution of the system a*x = b, Math.sqrt(par)*d*x = 0,
   *                     for the output par.
   * @param{Number[]} sdiag - is an OUTPUT ARRAY of length n which contains the
   *                         diagonal elements of the upper triangular matrix s.
   */
  mpLMParameter(n, r, ldr, ipvt, diag, qtb, delta, par, x, sdiag) {
    // compute and store in x the gauss-newton direction. if the
    // jacobian is rank-deficient, obtain a least squares solution.
    let nsing = n;
    let wa1 = [];
    const ifree = this.idxFree;

    for (let j = 0, jj = 0; j < n; j++, jj += ldr + 1) {
      wa1.push(qtb[j]);
      if (r[jj] == 0 && nsing == n)
        nsing = j;
      if (nsing < n)
        wa1[j] = 0;
    }

    if (nsing >= 1) {
      for (let k = 0; k < nsing; k++) {
        const j = nsing - k - 1;
        wa1[j] /= r[j + ldr * j];
        const temp = wa1[j];
        if (j - 1 >= 0)
          for (let i = 0, ij = ldr * j; i <= j - 1; i++, ij++)
            wa1[i] -= r[ij] * temp;
      }
    }

    wa1.forEach((val, idx) => { x[ipvt[idx]] = val; });

    // Evaluate the function at the origin,
    // and test for acceptance of the gauss-newton direction.
    let wa2 = x.map((val, idx) => (val * diag[ifree[idx]]));

    let dxnorm = this.mpEuclidNorm(n, wa2);
    let fp = dxnorm - delta;
    if (fp <= 0.1 * delta)
      par = 0;
    else {
      // if the jacobian is not rank deficient, the newton step provides a lower
      // bound, parLo, for the zero of the function. otherwise set this bound to zero.
      let paramLo = 0;
      if (nsing >= n) {
        for (let j = 0; j < n; j++)
          wa1[j] = diag[ifree[ipvt[j]]] * (wa2[ipvt[j]] / dxnorm);

        for (let j = 0, jj = 0; j < n; j++, jj += ldr) {
          let sum = 0;
          if (j - 1 >= 0) {
            let ij = jj;
            for (let i = 0; i <= j - 1; i++)
              sum += r[ij++] * wa1[i];
          }
          wa1[j] = (wa1[j] - sum) / r[j + ldr * j];
        }
        const temp = this.mpEuclidNorm(n, wa1);
        paramLo = ((fp / delta) / temp) / temp;
      }

      // calculate an upper bound, parHi, for the zero of the function.
      for (let j = 0, jj = 0; j < n; j++, jj += ldr) {
        let sum = 0;
        for (let i = 0, ij = jj; i <= j; i++, ij++)
          sum += r[ij] * qtb[i];

        wa1[j] = sum / diag[ifree[ipvt[j]]];
      }

      const gnorm = this.mpEuclidNorm(n, wa1);
      let paramHi = gnorm / delta;
      if (paramHi == 0)
        paramHi = MP_DWARF / Math.min(delta, 0.1);

      // if the input par lies outside of the interval (parlo,parhi),
      // set par to the closer endpoint.
      par = Math.max(par, paramLo);
      par = Math.min(par, paramHi);
      if (par == 0)
        par = gnorm / dxnorm;

      // beginning of an iteration.
      let iter = 0;
      const maxIter = 20;
      while (true) {
        iter++;
        //evaluate the function at the current value of par.
        if (par == 0)
          par = Math.max(MP_DWARF, 0.001 * paramHi);

        let temp = Math.sqrt(par);
        ifree.forEach((freeIdx, idx) => { wa1[idx] = temp * diag[freeIdx]; })
        // for (let j = 0; j < n; j++)
        //   wa1[j] = temp * diag[ifree[j]];

        this.mpQRSolve(n, r, ldr, ipvt, wa1, qtb, x, sdiag);
        wa2 = x.map((val, idx) => (val * diag[ifree[idx]]));

        dxnorm = this.mpEuclidNorm(n, wa2);
        temp = fp;
        fp = dxnorm - delta;

        // if the function is small enough, accept the current value
        // of par. also test for the exceptional cases where parl
        // is zero or the number of iterations has reached 10. */
        if ((Math.abs(fp) <= 0.1 * delta)
          || (paramLo == 0 && fp <= temp && temp < 0)
          || iter >= maxIter)
          break;

        // compute the newton correction.
        wa1 = wa1.map((val, idx) => (diag[ifree[ipvt[idx]]] * wa2[ipvt[idx]] / (dxnorm * sdiag[idx])));

        for (let j = 0, jj = 0; j < n; j++, jj += ldr) {
          temp = wa1[j];
          if (j + 1 < n)
            for (let i = j + 1, ij = j + 1 + jj; i < n; i++, ij++)
              wa1[i] -= r[ij] * temp; // ij: [i+ldr*j]
        }

        temp = this.mpEuclidNorm(n, wa1);
        const paramCorrection = ((fp / delta) / temp) / temp;
        // depending on the sign of the function, update parLo or parHi.
        if (fp > 0)
          paramLo = Math.max(paramLo, par);
        if (fp < 0)
          paramHi = Math.min(paramHi, par);

        // compute an improved estimate for par.
        par = Math.max(paramLo, par + paramCorrection);
      } // end of an iteration.
    }
    // last card of subroutine lmpar. - TML comment: So this was once a punch card program!
    // return the modified input parameters
    return { par, r, x, sdiag };
  }

  /************************fdjac2.c*************************/
  /** This subroutine computes a forward-difference approximation
   * to the m by n jacobian matrix associated with a specified
   * problem of m functions in n variables.
   *
   *   the value of iflag should not be changed by fcn unless
   *   the user wants to terminate execution of fdjac2.
   *   in this case set iflag to a negative integer.
   *
   * iflag is an integer variable which can be used to terminate
   *   the execution of fdjac2. see description of fcn.
   *
   * argonne national laboratory. minpack project. march 1980.
   * burton s. garbow, kenneth e. hillstrom, jorge j. more
   *
   * @param {Number} nData - is a positive integer input variable set to the number
   *                    of functions.
   * @param {Number[]} fitParams - is an input array of length n.
   * @param {Number[]} funcVals - is an input array of length m which must contain the
   *                         functions evaluated at x.
   * @param {Number[]} fjac - is AN OUTPUT m by n array which contains the
   *                         approximation to the jacobian matrix evaluated at x.
   * @param {Object} privData - void pointer to data that can be used by te user functon
   * @param {Number[]} step - npar long array abs step size for numeric derivative
   * @param {Number[]} dstep - npar long array relative step size for numeric derivative
   * @param {Number[]} diffSide - npar long array
   * @param {Boolean[]} hasHilimit - nfree long array
   * @param {Number[]} hiLimit - nfree long array
   * @param {Number[][]} dvec - pointer to pointer double - OUTPUT?
   * @returns {{iflag: Number, fjac: Number[], dvec: Number[][]}}
   */
  mpFDJacobi2(nData, fitParams, funcVals, fjac, privData,
    step, dstep, diffSide, hasHilimit, hiLimit, dvec) {
    /** define a return object
     * @param {Number} iflag
     * @param {Number[]} fjac
     * @param {Number[][]} dvec
     */
    const retObj = (iflag, fjac = null, dvec = null) => {
      return { iflag, fjac, dvec };
    };

    const nFree = this.freePars.length;
    // initialize the user computed dervative matrix
    dvec.fill(null);
    // Initialize the Jacobian derivative matrix
    fjac.fill(0);

    // Check for which parameters need analytical derivatives and which
    // need numerical ones
    let has_analytical_deriv = false;
    let has_numerical_deriv = false;
    // let has_debug_deriv = false;
    for (let j = 0; j < nFree; j++) {  // Loop through free parameters only
      if (diffSide[this.idxFree[j]] == 3) {
        // Purely analytical derivatives
        dvec[this.idxFree[j]] = fjac.slice(j * nData, nData); // Copy this bak into fjac afterwards
        has_analytical_deriv = true;
      } else
        has_numerical_deriv = true;
    }

    // If there are any parameters requiring analytical derivatives,
    // then compute them first.
    let wa = new Array(nData);
    if (has_analytical_deriv) {
      if (!this.userFunc(fitParams, wa, dvec, privData))
        return retObj(this._userFuncErrorCode);

      // TODO: THIS IS UNTESTED!!!
      // copy stuff from dvec back into fjac
      for (let j = 0; j < this.pars.length; j++)
        for (let i = 0; i < nData; i++)
          if (dvec[j])
            fjac[j * i + i] = dvec[j][i]; // IS THIS CORRECT??? OR OPPOSITE: [j * i + j]

      // if (iflag < 0)
      return retObj(this._userFuncErrorCode);
    }

    // jacobian matrix iterator
    // Parameters requiring numerical derivatives
    const eps = Math.sqrt(Math.max(this.conf.epsfcn, MP_MACHEP0));
    if (has_numerical_deriv)
      for (let j = 0, ij = 0; j < nFree; j++) {  // Loop thru free params
        let dsidei = (diffSide ? diffSide[this.idxFree[j]] : 0);

        // Skip parameters already done by user-computed partials
        if (diffSide && dsidei == 3) continue; // eslint-disable-line no-continue

        let temp = fitParams[this.idxFree[j]];
        let delta = eps * Math.abs(temp);
        // if (this.pars[ifree[j]].step > 0)
        if (step && step[this.idxFree[j]] > 0)
          delta = step[this.idxFree[j]];
        if (dstep && dstep[this.idxFree[j]] > 0)
          delta = Math.abs(dstep[this.idxFree[j]] * temp);
        if (delta == 0)
          delta = eps;

        // If negative step requested, or we are against the upper limit
        if ((diffSide && dsidei == -1)
          || (diffSide && dsidei == 0 && hasHilimit[j] && (temp > (hiLimit[j] - delta))))
          delta = -delta;

        fitParams[this.idxFree[j]] = temp + delta;
        if (!this.userFunc(fitParams, wa, null, privData))
          return retObj(this._userFuncErrorCode);

        fitParams[this.idxFree[j]] = temp;

        // Compute the one-sided derivative
        if (dsidei <= 1) {
          // if (!ddebug[ifree[j]]) {
          for (let i = 0; i < nData; i++, ij++)
            fjac[ij] = (wa[i] - funcVals[i]) / delta; // fjac[i+m*j]

        } else { // Compute the two-sided derivative - dside >= 2
          let wa2 = new Array(wa.length);
          wa.forEach((x, idx) => { wa2[idx] = x; });

          // Evaluate at x - h
          fitParams[this.idxFree[j]] = temp - delta;
          if (!this.userFunc(fitParams, wa, null, privData))
            return retObj(this._userFuncErrorCode);

          fitParams[this.idxFree[j]] = temp;

          // Now compute derivative as (f(x+h) - f(x-h))/(2h)
          //if (!ddebug[ifree[j]])
          for (let i = 0; i < nData; i++, ij++)
            fjac[ij] = 0.5 * ((wa2[i] - wa[i])) / delta;

        } // end if (dside >= 2)
      } // end if (has_numerical_derivative)

    if (this._userFuncErrorCode != kMPFitStatus.MP_GOOD)
      return retObj(this._userFuncErrorCode);

    return retObj(this._userFuncErrorCode, fjac, dvec);
    // last card of subroutine fdjac2.
  }

  /************************qrfac.c*************************/
  /** This subroutine uses householder transformations with column
   *  pivoting (optional) to compute a QR factorization of the
   *  m by n matrix a. that is, qrfac determines an orthogonal
   *  matrix q, a permutation matrix p, and an upper trapezoidal
   *  matrix r with diagonal elements of nonincreasing magnitude,
   *  such that a*p = q*r. the householder transformation for
   *  column k, k = 1,2,...,min(m,n), is of the form
   *
   *       i - (1 / u(k)) * u^2
   *
   *  where u has zeros in the first k-1 positions. the form of
   *  this transformation and the method of pivoting first
   *  appeared in the corresponding linpack subroutine.
   *
   *  argonne national laboratory. minpack project. march 1980.
   *  burton s. garbow, kenneth e. hillstrom, jorge j. more
   *
   * @param {Number} m - is a positive integer input variable set to the number
   *                     of rows of a.
   * @param {Number} n - is a positive integer input variable set to the number
   *                     of columns of a.
   * @param {Number[]} a - is an m by n array. on input a contains the matrix for
   *                       which the qr factorization is to be computed. ON OUTPUT
   *                       the strict upper trapezoidal part of a contains the strict
   *                       upper trapezoidal part of r, and the lower trapezoidal
   *                       part of a contains a factored form of q (the non-trivial
   *                       elements of the u vectors described above).
   * @param {Boolean} pivot - pivot is a logical input variable. if pivot is set true,
   *                          then column pivoting is enforced. if pivot is set false,
   *                          then no column pivoting is done.
   * @param {Number[]} ipvt - is an INTEGER OUTPUT ARRAY of length lipvt. ipvt
   *                          defines the permutation matrix p such that a*p = q*r.
   *                          column j of p is column ipvt(j) of the identity matrix.
   *                          if pivot is false, ipvt is not referenced.
   * @param {Number[]} rdiag - is an OUTPUT ARRAY of length n which contains the
   *                           diagonal elements of r.
   * @param {Number[]} acnorm - is an OUTPUT ARRAY of length n which contains the
   *                            norms of the corresponding columns of the input matrix a.
   *                            if this information is not needed, then acnorm can coincide
   *                            with rdiag.
   */
  mpQRFactorize(m, n, a, pivot, ipvt, rdiag, acnorm) {
    // compute the initial column norms and initialize several arrays.
    let wa = new Array(n);
    for (let j = 0, ij = 0; j < n; j++, ij += m) {
      acnorm[j] = this.mpEuclidNorm(m, a.slice(ij, ij + m)); // &a[ij]
      rdiag[j] = acnorm[j];
      wa[j] = rdiag[j];
      if (pivot)
        ipvt[j] = j;
    }

    // reduce a to r with householder transformations.
    const minmn = Math.min(m, n);
    for (let j = 0; j < minmn; j++) {
      if (pivot) {
        // bring the column of largest norm into the pivot position.
        let kmax = j;
        for (let k = j; k < n; k++) {
          if (rdiag[k] > rdiag[kmax])
            kmax = k;
        }
        if (kmax != j) {
          for (let i = 0, ij = m * j, jj = m * kmax; i < m; i++, ij++, jj++) {
            const temp = a[ij]; // [i+m*j]
            a[ij] = a[jj]; // [i+m*kmax]
            a[jj] = temp;
          }
          rdiag[kmax] = rdiag[j];
          wa[kmax] = wa[j];
          [ipvt[j], ipvt[kmax]] = [ipvt[kmax], ipvt[j]];
        }
      }

      // compute the householder transformation to reduce the
      // j-th column of a to a multiple of the j-th unit vector.
      const jj = j + m * j;
      let ajnorm = this.mpEuclidNorm(m - j, a.slice(jj, jj + m - j)); // &a[jj]
      if (ajnorm != 0) {
        if (a[jj] < 0)
          ajnorm = -ajnorm;

        for (let i = j, ij = jj; i < m; i++, ij++)
          a[ij] /= ajnorm;

        a[jj] += 1.0;

        // apply the transformation to the remaining columns and update the norms.
        const jp1 = j + 1;
        if (jp1 < n) {
          for (let k = jp1; k < n; k++) {
            let sum = 0.0;
            for (let i = j, ij = j + m * k, jj = j + m * j; i < m; i++, ij++, jj++)
              sum += a[jj] * a[ij];

            let temp = sum / a[j + m * j];
            for (let i = j, ij = j + m * k, jj = j + m * j; i < m; i++, ij++, jj++)
              a[ij] -= temp * a[jj];

            if (pivot && (rdiag[k] != 0)) {
              temp = a[j + m * k] / rdiag[k];
              temp = Math.max(0.0, 1 - temp * temp);
              rdiag[k] *= Math.sqrt(temp);
              temp = rdiag[k] / wa[k];

              if ((0.05 * temp * temp) <= MP_MACHEP0) {
                rdiag[k] = this.mpEuclidNorm(m - j - 1, a.slice(jp1 + m * k, jp1 + m * k + m - j - 1)); // &a[jp1 + m * k]);
                wa[k] = rdiag[k];
              }
            }
          }
        }
      }
      rdiag[j] = -ajnorm;
    }
    // last card of subroutine qrfac. TML comment: Once upon a time translated from "Punch Cards"
    // return the modified arrays/matrixes
    return { a, ipvt, rdiag, acnorm };
  }

  //_____________________________________________________________________________
  /** The main fitting function m, npar, xall, fvec, 0, private_data);
   * @param {Number} nData - is a positive integer input variable set to the number
   *                         of functions (TML: Data points to be fitted with user provided function).
   * @param {MPParameter[]} [pars] - array of npar structures specifying constraints. or 0 (null pointer)
   *                               for unconstrained fitting [ see README and mpfit.h for definition & use of mp_par]
   * @param {MPConfig} [conf]
   * @param {Object} [privData] - user provided object passed on to the user function
   * @returns [Number] - integer is the fit result
   */
  mpFit(nData, pars = null, conf = null, privData = null) {
    /** Define a return object
     * @param {Number} code
     * @param {Number[]} params
     * @param {MPResult} result
     */
    const retObj = (code, params = null, result = null) => {
      return { info: getMPError(code), code, params, result };
    };

    // initialize the parameter structure
    this.init();

    // replace parameters if new provided
    if (pars) this.pars = pars;

    // replace config if provided
    if (conf)
      this.conf = conf;
    else if (this.conf)
      conf = this.conf;
    else { // create a default config
      this.conf = new MPConfig();
      conf = this.conf;
    }

    // check config
    conf.checkCongfig();

    const npar = this.pars.length;
    const nfree = this.freePars.length;
    const ifree = this.idxFree;

    // Basic error checking
    if (npar === 0 && nData === 0) return retObj(kMPFitStatus.MP_ERR_INPUT);
    if (npar <= 0) return retObj(kMPFitStatus.MP_ERR_PARAM);
    if (nData <= 0) return retObj(kMPFitStatus.MP_ERR_NPOINTS);

    // Find the free parameters
    // const ifree = this.pars.map((par, idx) => (!par.fixed ? idx : -1)).filter(val => val != -1);
    // const nfree = ifree.length;

    // Ensure there are some degrees of freedom
    if (nfree == 0) return retObj(kMPFitStatus.MP_ERR_NFREE);
    if (nData < nfree) return retObj(kMPFitStatus.MP_ERR_DOF);

    // Sanity checking on input configuration */
    if (!conf || npar <= 0 || conf.chi2Tolerance <= 0 || conf.paramTolerance <= 0
      || conf.orthoTolerance <= 0 || conf.maxiter < 0 || conf.stepfactor <= 0)
      return retObj(kMPFitStatus.MP_ERR_CONF);

    // create arrays: Finite differencing step, absolute and relative, and sidedness of deriv
    const step = this.pars.map(par => par.deltaStep); // new Array(npar); // doubles
    const relStep = this.pars.map(par => par.relDeltaStep); //new Array(npar); // doubles
    const mpside = this.pars.map(par => par.side); // new Array(npar); // integers

    if (this.pars.filter(par => !par.withinBounds()).length > 0)
      return retObj(kMPFitStatus.MP_ERR_BOUNDS);

    // copy limits
    const loLim = this.freePars.map(par => par.lowLimit); // new Array(nfree);
    const hiLim = this.freePars.map(par => par.hiLimit); // new Array(nfree);
    const hasLoLim = this.freePars.map(par => par.lowLimited); // new Array(nfree);
    const hasHiLim = this.freePars.map(par => par.hiLimited); // new Array(nfree);
    const anyLimits = Boolean(hasLoLim.find(x => x) || hasHiLim.find(x => x));

    /* Allocate temporary storage */
    let residuals = new Array(nData);
    let qtf = new Array(nfree);
    let jacobi = new Array(nData * nfree);
    let diag = new Array(npar);
    let wa1 = new Array(npar);
    let wa2 = new Array(npar);
    let wa3 = new Array(npar);
    let wa4 = new Array(nData);
    let ipvt = new Array(npar);
    let dvecptr = new Array(npar); // m x n matrix

    for (let i = 0; i < npar; i++)
      dvecptr[i] = new Array(nData);

    qtf.fill(0);

    const leadDimFjac = nData; // first coordinate size since fjac is in reality a matrix

    // Evaluate user function with initial parameter values
    let allFitParams = this.pars.map(par => (par.value));
    if (!this.userFunc(allFitParams, residuals, null, privData))
      return retObj(this._userFuncErrorCode);

    // Make a new copy
    let newFitParams = allFitParams.map(x => x);

    // Transfer only the free parameters
    let fitParams = ifree.map(freeIdx => allFitParams[freeIdx]);

    // set normalization numbers
    let residNorm = this.mpEuclidNorm(nData, residuals);
    let origNorm = residNorm * residNorm;
    let fnorm1 = -1;
    let xnorm = 1;
    // Initialize Levelberg-Marquardt parameter and iteration counter
    let lmPar = 0;
    let delta = 0;
    let iter = 1;
    let info = kMPFitStatus.MP_GOOD;

    // Beginning of the outer loop
    let runOuterLoop = true;
    while (runOuterLoop) {
      for (let i = 0; i < nfree; i++)
        newFitParams[ifree[i]] = fitParams[i];

      // Calculate the jacobian matrix
      const jacobiRes = this.mpFDJacobi2(nData, newFitParams, residuals, jacobi,
        privData, step, relStep, mpside, hasHiLim, hiLim, dvecptr);
      // console.log('After fdJac2 - fjac: ' + JSON.stringify(fjac));

      if (jacobiRes && jacobiRes.iflag != 0)
        return retObj(jacobiRes.iflag);
      // console.log('After fdJac2 - fjac: ' + JSON.stringify(jacobiRes.fjac));

      // Determine if any of the parameters are pegged at the limits
      if (anyLimits) {
        for (let j = 0; j < nfree; j++) {
          const lowPegged = (hasLoLim[j] && (fitParams[j] == loLim[j]));
          const hiPegged = (hasHiLim[j] && (fitParams[j] == hiLim[j]));
          let sum = 0;

          // If the parameter is pegged at a limit, compute the gradient direction
          if (lowPegged || hiPegged)
            residuals.forEach((val, idx) => { sum += val * jacobi[j * leadDimFjac + idx]; });

          // If pegged at lower limit and gradient is toward negative then
          // reset gradient to zero
          if ((lowPegged && sum > 0) || (hiPegged && sum < 0))
            jacobi.fill(0, j * leadDimFjac, j * leadDimFjac + nData);
        }
      }

      // Compute the QR factorization of the jacobian
      this.mpQRFactorize(nData, nfree, jacobi, true, ipvt, wa1, wa2);

      // on the first iteration and if mode is 1, scale according
      // to the norms of the columns of the initial jacobian.
      if (iter == 1) {
        if (!conf.doUserScale)
          for (let j = 0; j < nfree; j++)
            diag[ifree[j]] = wa2[j] || 1.0;
        // console.log(JSON.stringify(diag));
        // on the first iteration, calculate the norm of the scaled x
        // and initialize the step bound delta.
        wa3 = ifree.map((freeIdx, idx) => diag[freeIdx] * fitParams[idx]);

        xnorm = this.mpEuclidNorm(nfree, wa3);
        delta = (conf.stepfactor * xnorm) || conf.stepfactor;
      }

      // form (q transpose)*fvec and store the first n components in qtf.
      residuals.forEach((val, idx) => { wa4[idx] = val; });

      for (let j = 0, jj = 0; j < nfree; j++, jj += nData + 1) {
        if (jacobi[jj] != 0) {
          let sum = 0;
          for (let i = j, ij = jj; i < nData; i++, ij++)
            sum += jacobi[ij] * wa4[i]; // fjac[i+m*j]

          let temp = -sum / jacobi[jj];
          for (let i = j, ij = jj; i < nData; i++, ij++)
            wa4[i] += jacobi[ij] * temp; // fjac[i+m*j]
        }

        jacobi[jj] = wa1[j];
        qtf[j] = wa4[j];
      }

      // From here, only the square matrix, consisting of triangle of R, is needed.
      if (conf.finiteCheck)
        // Check for overflow.  This should be a cheap test here since FJAC
        // has been reduced to a (small) square matrix, and the test is O(N^2).
        for (let j = 0, off = 0; j < nfree; j++, off += leadDimFjac)
          for (let i = 0; i < nfree; i++)
            if (!isFinite(jacobi[off + i]))
              return retObj(kMPFitStatus.MP_ERR_NAN);

      // compute the norm of the scaled gradient.
      let gnorm = 0;
      if (residNorm != 0)
        for (let j = 0, jj = 0; j < nfree; j++, jj += nData)
          if (wa2[ipvt[j]] != 0) {
            let sum = 0;
            for (let i = 0, ij = jj; i <= j; i++, ij++)
              sum += jacobi[ij] * (qtf[i] / residNorm); // fjac[i+m*j]

            gnorm = Math.max(gnorm, Math.abs(sum / wa2[ipvt[j]]));
          }

      // test for convergence of the gradient norm.
      if (gnorm <= conf.orthoTolerance)
        info = kMPFitStatus.MP_OK_DIR;

      else if (conf.maxiter == 0)
        info = kMPFitStatus.MP_MAXITER;

      if (info != kMPFitStatus.MP_GOOD)
        break;
      // rescale if necessary.
      if (!conf.doUserScale)
        for (let j = 0; j < nfree; j++)
          diag[ifree[j]] = Math.max(diag[ifree[j]], wa2[j]);

      // beginning of the inner loop.
      while (true) {
        // determine the levenberg-marquardt parameter.
        lmPar = this.mpLMParameter(nfree, jacobi, leadDimFjac, ipvt, diag, qtf, delta, lmPar, wa1, wa2).par;
        //console.log("After LMPAR - Par: %f\n", par);

        // store the direction p and x + p. calculate the norm of p.
        wa1 = wa1.map(x => (-x));

        let alpha = 1.0;
        // If no parameter limits, so just move to new position WA2
        if (!anyLimits)
          wa2 = fitParams.map((val, idx) => (val + wa1[idx])); // eslint-disable-line no-loop-func
        else {
          // Respect the limits. If a step were to go out of bounds, then
          // we should take a step in the same direction but shorter distance.
          // The step should take us right to the limit in that case. */
          for (let j = 0; j < nfree; j++) {
            const lpegged = (hasLoLim[j] && (fitParams[j] <= loLim[j]));
            const upegged = (hasHiLim[j] && (fitParams[j] >= hiLim[j]));
            const dwa1 = (Math.abs(wa1[j]) > MP_MACHEP0);

            if ((lpegged && wa1[j] < 0)
              || (upegged && wa1[j] > 0))
              wa1[j] = 0;

            if (dwa1 && hasLoLim[j] && ((fitParams[j] + wa1[j]) < loLim[j]))
              alpha = Math.min(alpha, (loLim[j] - fitParams[j]) / wa1[j]);

            if (dwa1 && hasHiLim[j] && ((fitParams[j] + wa1[j]) > hiLim[j]))
              alpha = Math.min(alpha, (hiLim[j] - fitParams[j]) / wa1[j]);
          }

          // Scale the resulting vector, advance to the next position
          wa1 = wa1.map(val => val * alpha);
          wa2 = wa1.map((val, idx) => val + fitParams[idx]);
          for (let j = 0; j < nfree; j++) {
            // Adjust the output values.  If the step put us exactly
            // on a boundary, make sure it is exact.
            if (hasHiLim[j]) {
              const ulim1 = hiLim[j] * (1 - (hiLim[j] >= 0 ? 1 : -1) * MP_MACHEP0) - ((hiLim[j] == 0) ? MP_MACHEP0 : 0);
              if (wa2[j] >= ulim1)
                wa2[j] = hiLim[j];
            }
            if (hasLoLim[j]) {
              const llim1 = loLim[j] * (1 + (loLim[j] >= 0 ? 1 : -1) * MP_MACHEP0) + ((loLim[j] == 0) ? MP_MACHEP0 : 0);
              if (wa2[j] <= llim1)
                wa2[j] = loLim[j];
            }
          }
        }

        wa3 = ifree.map((freeIdx, idx) => diag[freeIdx] * wa1[idx]);
        const pnorm = this.mpEuclidNorm(nfree, wa3);

        // on the first iteration, adjust the initial step bound.
        if (iter == 1)
          delta = Math.min(delta, pnorm);

        // evaluate the function at x + p and calculate its norm.
        for (let i = 0; i < nfree; i++)
          newFitParams[ifree[i]] = wa2[i];

        if (!this.userFunc(newFitParams, wa4, null, privData)) {
          runOuterLoop = false;
          break;
        }

        fnorm1 = this.mpEuclidNorm(nData, wa4);

        // compute the scaled actual reduction.
        const actualReduction = (0.1 * fnorm1 < residNorm
          ? 1.0 - (fnorm1 / residNorm) ** 2
          : -1.0);

        // compute the scaled predicted reduction and the scaled directional derivative.
        for (let j = 0, jj = 0; j < nfree; j++, jj += nData) {
          wa3[j] = 0;
          let temp = wa1[ipvt[j]];

          for (let i = 0, ij = jj; i <= j; i++, ij++)
            wa3[i] += jacobi[ij] * temp; // fjac[i+m*j]
        }

        // Remember, alpha is the fraction of the full LM step actually taken
        const temp1 = this.mpEuclidNorm(nfree, wa3) * alpha / residNorm;
        const temp2 = (Math.sqrt(alpha * lmPar) * pnorm) / residNorm;
        const predictedReduction = temp1 * temp1 + (temp2 * temp2) / 0.5;
        const dirder = -(temp1 * temp1 + temp2 * temp2);
        // console.log("\ntemp1: %f, temp2, %f. fnorm: %f alpha: %f par: %f\n", temp1, temp2, fnorm, alpha, par);

        // compute the ratio of the actual to the predicted reduction.
        const ratio = (predictedReduction != 0 ? actualReduction / predictedReduction : 0);

        // update the step bound.
        if (ratio <= 0.25) {
          let temp = 0.5 * (actualReduction >= 0 ? 1 : dirder / (dirder + 0.5 * actualReduction));

          if (0.1 * fnorm1 >= residNorm || temp < 0.1)
            temp = 0.1;

          delta = temp * Math.min(delta, pnorm / 0.1);
          lmPar /= temp;
        } else if (lmPar == 0 || ratio >= 0.75) {
          delta = pnorm / 0.5;
          lmPar *= 0.5;
        }

        // test for successful iteration.
        if (ratio >= 1e-4) {
          // successful iteration. update x, fvec, and their norms.
          fitParams = wa2.map(x => x);
          wa2 = ifree.map((freeIdx, idx) => (diag[freeIdx] * fitParams[idx]));
          residuals = wa4.map(val => val);
          xnorm = this.mpEuclidNorm(nfree, wa2);
          residNorm = fnorm1;
          iter++;
        }

        // tests for convergence.
        if ((Math.abs(actualReduction) <= conf.chi2Tolerance) && (predictedReduction <= conf.chi2Tolerance)
          && (0.5 * ratio <= 1))
          info = kMPFitStatus.MP_OK_CHI;

        if (delta <= conf.paramTolerance * xnorm)
          info = kMPFitStatus.MP_OK_PAR;

        if ((Math.abs(actualReduction) <= conf.chi2Tolerance) && (predictedReduction <= conf.chi2Tolerance)
          && (0.5 * ratio <= 1)
          && (info == kMPFitStatus.MP_OK_PAR))
          info = kMPFitStatus.MP_OK_BOTH;

        if (info != kMPFitStatus.MP_GOOD) {
          runOuterLoop = false;
          break;
        }

        // tests for termination and stringent tolerances.
        if (iter >= conf.maxiter || (conf.maxFuncEval > 0 && this._nFuncEvals >= conf.maxFuncEval))
          info = kMPFitStatus.MP_MAXITER;

        if ((Math.abs(actualReduction) <= MP_MACHEP0) && (predictedReduction <= MP_MACHEP0)
          && (0.5 * ratio <= 1.0))
          info = kMPFitStatus.MP_CHI2TOL;

        if (delta <= MP_MACHEP0 * xnorm)
          info = kMPFitStatus.MP_PARAMTOL;

        if (gnorm <= MP_MACHEP0)
          info = kMPFitStatus.MP_ORTHOTOL;

        if (info != kMPFitStatus.MP_GOOD) {
          runOuterLoop = false;
          break;
        }

        if (ratio >= 1e-4) break;
      } // end of the inner loop. repeat if iteration unsuccessful.
    } // end of the outer loop.

    // termination
    if (this._userFuncErrorCode < 0)
      info = this._userFuncErrorCode;

    for (let i = 0; i < nfree; i++)
      allFitParams[ifree[i]] = fitParams[i];

    // is this needed? Printing updated result id we have errrors?
    if (conf.nprint > 0 && info > 0)
      this.userFunc(allFitParams, residuals, null, privData);

    // Compute number of pegged parameters
    this.pars.forEach((par, i) => {
      par.pegged = ((par.lowLimited && par.lowLimit == allFitParams[i])
        || (par.hiLimited && par.hiLimit == allFitParams[i]));
    });

    const npegged = this.pars.filter(par => (par.pegged)).length;

    // Compute and return the covariance matrix and/or parameter errors
    this.mpCovariance(nfree, jacobi, leadDimFjac, ipvt, conf.covTolerance);

    // Transfer the covariance array
    let result = new MPResult(this.pars.length);

    for (let j = 0; j < nfree; j++)
      for (let i = 0; i < nfree; i++)
        result.covar[ifree[j] * npar + ifree[i]] = jacobi[j * leadDimFjac + i];

    // copy fitted params back into pars array
    allFitParams.forEach((par, idx) => { this.pars[idx].value = par; });

    result.version = kMPFitStatus.MPFIT_VERSION;
    result.chi2 = (Math.max(residNorm, fnorm1)) ** 2;
    result.chi2Start = origNorm;
    result.status = info;
    result.niter = iter;
    result.nFuncEvals = this._nFuncEvals;
    result.nFuncMemo = this._nStoreRetrievals;
    result.nFreePars = nfree;
    result.nPeggedPars = npegged;
    result.nResiduals = nData;
    result.nDof = nData - nfree;
    result.params = this.pars;

    for (let j = 0; j < nfree; j++)
      if (jacobi[j * leadDimFjac + j] > 0)
        result.params[ifree[j]].uncert = Math.sqrt(jacobi[j * leadDimFjac + j]);

    if (!this.conf.hasResidUncert && result.chi2 / (result.nResiduals - result.nFreePars) < 1)
      result.params.forEach(par => {
        par.uncert = par.value * Math.sqrt(result.chi2 / result.nDof)
      });

    // Copy residuals if requested
    if (result.residuals)
      residuals.forEach((x, idx) => { result.residuals[idx] = x; });

    return retObj(info, allFitParams, result);
  }
}
//=============================================================================
/** Simplified implementation - run with default parameters
 * @param {Function} userFunction - the function shall compute the residulas between function and each data point
 * @param {Number} nData - the number of data points
 * @param {MPParameter | MPParameter[]} pars
 * @param {MPConfig} [config]
 * @param {Object} [privObj]
 */
export function mpFit(userFunction, nData, pars, config = null, privObj = null) {
  if (!pars) {
    printConsoleError('MP Fit algoruthms needs at least 1 parameter: "MPParameter"');
    return null;
  }

  if ((pars instanceof Array && !(pars[0] instanceof MPParameter))
    || (!(pars instanceof Array) && !(pars instanceof MPParameter))) {
    printConsoleError('Parameters must be of type "MPParameter"');
    return null;
  }

  const parArray = (!(pars instanceof Array) ? [pars] : pars);

  let mpfit = new MPFit(userFunction, parArray, true);

  if (config == null)
    config = new MPConfig();

  return mpfit.mpFit(nData, null, config, privObj);
}

//=============================================================================
/** Simplified implementation - run with default parameters
 * @param {Function} userFunction - the function shall compute the residulas between function and each data point
 * @param {Number} nData - the number of data points
 * @param {Number | Number[]} pars - the fit parameters
 * @param {Object} [privObj] - this object will be passed on to the userFunction
 * @param {Number | Number[]} [lowLimit] - the optional low limit of each fit parameter
 * @param {Number | Number[]} [hiLimit] - the optional upper limit of each fit parameter
 */
export default function mpFitSimple(userFunction, nData, pars, privObj = null,
  lowLimit = null, hiLimit = null) {

  if (pars === undefined || (typeof pars !== 'number' && pars.length == 0)) {
    printConsoleError('MP Fit algoruthms needs at least 1 parameter!');
    return null;
  }

  // check length of arrays
  if (typeof pars !== 'number') {
    if (lowLimit !== null && lowLimit !== undefined
      && typeof lowLimit !== 'number' && lowLimit.length !== pars.length) {
      printConsoleError('Provided parameter low limit array is not of the same size as the parameters array!');
      return null;
    }
    if (hiLimit !== null && hiLimit !== undefined
      && typeof hiLimit !== 'number' && hiLimit.length !== pars.length) {
      printConsoleError('Provided parameter high limit array is not of the same size as the parameters array!');
      return null;
    }
  }

  /** @type{MPParameter[]} */
  const parArray = [];
  if (typeof pars === 'number') {
    parArray.push(new MPParameter());

  } else
    pars.forEach((val, idx) => {
      parArray.push(new MPParameter());
      parArray[idx].value = val;
      if (lowLimit) {
        parArray[idx].lowLimit = lowLimit[idx];
        parArray[idx].lowLimited = true;
      }
      if (hiLimit) {
        parArray[idx].hiLimit = hiLimit[idx];
        parArray[idx].hiLimited = true;
      }
    });

  let mpfit = new MPFit(userFunction, parArray, true);

  const config = new MPConfig();

  return mpfit.mpFit(nData, null, config, privObj);
}

//_____________________________________________________________________________
//
// EOF
//
