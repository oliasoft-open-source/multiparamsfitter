# Multi Params Fitter Release Notes

## 0.3.3

- Upgrade to Node 22

## 0.3.2

- Fix mattermost url and changed channel name to Release bots [OW-14760](https://oliasoft.atlassian.net/browse/OW-14760)

## 0.3.1

- Fix release CI/CD pipeline ([OW-11691](https://oliasoft.atlassian.net/browse/OW-11691))

## 0.3.0

- first publish of release notes
